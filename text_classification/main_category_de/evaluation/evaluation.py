import glob
import pickle
import sys, os



PROJECT_FOLDER = os.path.dirname(os.path.abspath(os.getcwd()))
PROJECT_FOLDER = PROJECT_FOLDER.replace("text_classification/main_category_de", "")
sys.path.append(PROJECT_FOLDER)

PROJECT_FOLDER = os.path.dirname(os.path.abspath(os.getcwd()))
PROJECT_FOLDER = PROJECT_FOLDER.replace("text_classification/main_category_de", "")
sys.path.append(PROJECT_FOLDER)
from text_classification.main_category_de.data_utils.data_utils import DataLoaderCsvTextClassification
from text_classification.main_category_de.pre_processing.pre_processing import PreProcessing
import numpy as np
from absl import flags
from absl import app
from keras.preprocessing.sequence import pad_sequences
from tensorflow.keras import models

FLAGS = flags.FLAGS
flags.DEFINE_string("input_data_set", "", "path to the training data set")
flags.DEFINE_string("input_trained_modelAndDicts", "",
                    "The model and dictionaries files you will use for the predictions")
flags.DEFINE_integer("batch_size", 32, "batch size")
flags.DEFINE_string("csv_delimiter", "", "delimiter used in the training data set. If it is \t please write tab")
flags.DEFINE_integer("position_class", None, "position's index of the classes")
flags.DEFINE_integer("position_text", None, "position's index of the texts")
flags.DEFINE_string("optimizer", "", "sgd rmsprop adagrad adadelta adam adamax nadam")
flags.DEFINE_string("label_delimiter", "", "Delimiter used to split the different possible labels")


def main(argv):
    data_loader: DataLoaderCsvTextClassification = DataLoaderCsvTextClassification(input_path=FLAGS.input_data_set,
                                                                                   position_text=FLAGS.position_text,
                                                                                   position_label=FLAGS.position_class,
                                                                                   input_path_delimiter=FLAGS.csv_delimiter,
                                                                                   label_delimiter=FLAGS.label_delimiter)

    modelDictionaries_directory = glob.glob(os.path.join(FLAGS.input_trained_modelAndDicts, "*"))
    for file in modelDictionaries_directory:
        if "id2label.pck" in file:
            with open(file, "rb") as o:
                id2label = pickle.load(o)
                print(id2label)
        if "word2id.pck" in file:
            with open(file, "rb") as o:
                word2id = pickle.load(o)
        if "model.h5" in file:
            model = models.load_model(file)

    testSet = data_loader.load_data_set()
    pre_processor = PreProcessing(testSet)

    testSet = pre_processor.data_set_2_matrices(testSet, pre_processor.word2id, pre_processor.label2id,
                                                multi_label=True)
    print(testSet[10])
    x_test = []
    y_test = []
    for data in testSet:
        text, label = data
        x_test.append(text)
        y_test.append(label)
    MAX_LENGTH_TEXT = 150
    x_test = np.asarray(x_test)
    # add padding
    X_test = pad_sequences(x_test, MAX_LENGTH_TEXT, dtype='int32', padding='pre', truncating='pre', value=0)
    Y_test = np.asarray(y_test)
    # 3 Train the model
    print("Start Evaluation")
    history = model.evaluate(X_test, Y_test,
                             verbose=1,
                             batch_size=256)


if __name__ == "__main__":
    app.run(main)
