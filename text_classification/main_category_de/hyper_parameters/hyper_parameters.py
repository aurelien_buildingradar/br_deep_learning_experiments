class HyperParameter:
    def __init__(self):
        self.hyper_parameter: dict = self.create_hyper_parameters()

    def create_hyper_parameters(self):
        hp = {"embedding_length": 300,
              "lstm_units": 256,
              "lr": 0.001,
              "max_length": 150,
              "batch_size": 32,
              "number_epochs": 1,
              "number_hidden_lstm_layers": 2}
        return hp
