import datetime
import os, sys
import pickle
from collections import defaultdict
from random import shuffle
import sklearn
from sklearn.model_selection import train_test_split

PROJECT_FOLDER = os.path.dirname(os.path.abspath(os.getcwd()))
PROJECT_FOLDER = PROJECT_FOLDER.replace("text_classification/main_category_de", "")
sys.path.append(PROJECT_FOLDER)

import keras
from keras.callbacks import ReduceLROnPlateau, EarlyStopping
from keras_preprocessing.sequence import pad_sequences
from text_classification.main_category_de.data_utils.data_utils import DataLoaderCsvTextClassification
from text_classification.main_category_de.hyper_parameters.hyper_parameters import HyperParameter
from text_classification.main_category_de.model_architectures.model_architectures import lstm_model
from text_classification.main_category_de.pre_processing.pre_processing import PreProcessing
import numpy as np

root_folder = PROJECT_FOLDER
print(root_folder)
from absl import flags
from absl import app

FLAGS = flags.FLAGS
flags.DEFINE_string("input_data_set", "", "path to the training data set")
flags.DEFINE_string('output_trained_modelAndDicts', 'noName',
                    'where the model and the dictionary will be saved (list of '
                    'objects)')
flags.DEFINE_integer("batch_size", 32, "batch size")


def label_count(LABEL_NAME, label_list, label_dict):
    if LABEL_NAME in label_list:
        if len(label_list) not in label_dict:
            label_dict[len(label_list)] = 1
        else:
            label_dict[len(label_list)] += 1

    return label_dict


# flags.DEFINE_string("csv_delimiter", "", "delimiter used in the training data set. If it is \t please write tab")
# flags.DEFINE_integer("position_class", None, "position's index of the classes")
# flags.DEFINE_integer("position_text", None, "position's index of the texts")
# flags.DEFINE_integer("number_epochs", 10, "Number of epochs")
# flags.DEFINE_string("loss", "", "binary or categorical crossentropy")
# flags.DEFINE_string("optimizer", "", "sgd rmsprop adagrad adadelta adam adamax nadam")
# flags.DEFINE_string("units", "", "Number of gru/lstm unit")
# flags.DEFINE_string("embedding_length", "", "Size of the embeddings")
# flags.DEFINE_string("dropout", "", "Gru/lstm dropout")
# flags.DEFINE_string("recurrent_dropout", "", "Gru/lstm recurrent dropout")

def main(argv):
    # todo create train,dev,test sets
    input_path: str = FLAGS.input_data_set

    position_text: int = 2
    position_label: int = 3
    input_path_delimiter: str = ","
    class_delimiter: str = ";"
    hyper_parameters = HyperParameter()
    data_loader = DataLoaderCsvTextClassification(input_path=input_path, position_text=position_text,
                                                  position_label=position_label,
                                                  input_path_delimiter=input_path_delimiter,
                                                  label_delimiter=class_delimiter)
    freq_labels = {}
    freq_count = {}
    freq_ofc = {}
    data_set = data_loader.load_data_set()
    for el in data_set:
        t, labels = el
        if len(labels) not in freq_count:
            freq_count[len(labels)] = 1
        else:
            freq_count[len(labels)] += 1
        if len(labels) == 4:
            print("=" * 50)
            print("LABEL: ", labels)
            print("-" * 50)
            print(t)
        for label in labels:
            if label not in freq_labels:
                freq_labels[label] = 1
            else:
                freq_labels[label] += 1

        list_dict = [{"name": label} for label in list(freq_labels.keys())]
        for label in labels:
            print(label)
            list_dict[label] = label_count(LABEL_NAME=label, label_list=labels, label_dict=list_dict[label])

        for label_dict in list_dict:
            print("LABEL: ", label_dict["name"])
            print(label_dict)
    print(U)
    print("+" * 100, freq_labels.items())
    print(freq_count)
    shuffle(data_set)
    print(len(data_set))
    print(data_set[10] + 2)

    # pre process data
    # pre process string ?
    # put input into int matrices
    pre_processor = PreProcessing(data_set)
    word2id, label2id = pre_processor.create_x2id(data_set)
    print("Vocab size: ", len(word2id))
    print(label2id)
    training_set = pre_processor.data_set_2_matrices(data_set, pre_processor.word2id, pre_processor.label2id,
                                                     multi_label=True)

    print(training_set[10])
    id2word = {v: k for k, v in word2id.items()}
    id2label = {v: k for k, v in label2id.items()}

    x_train = []  # list of the text
    y_train = []  # list of the labels

    for data in training_set:
        text, label = data

        x_train.append(text)
        y_train.append(label)
    x_train = np.asarray(x_train)
    # add padding
    X_train = pad_sequences(x_train, hyper_parameters.hyper_parameter["max_length"], dtype='int32', padding='post',
                            truncating='post'
                            , value=word2id["PADDING_TOKEN"])
    Y_train = np.array(y_train)

    print(label2id.keys())
    if len(label2id) == 2:
        compileMode = "binary"
    else:
        compileMode = "categorical"

    # model

    model = lstm_model(VOCAB_SIZE=len(word2id), EMBEDDING_LENGTH=hyper_parameters.hyper_parameter["embedding_length"],
                       MAX_LENGTH=hyper_parameters.hyper_parameter["max_length"], NUM_CLASS=len(label2id),
                       COMPILE_MODE=compileMode, UNITS=hyper_parameters.hyper_parameter["lstm_units"],
                       number_hidden_lstm_layers=hyper_parameters.hyper_parameter["number_hidden_lstm_layers"],
                       multi_label=True)

    # Train the model

    model_trained_directory = FLAGS.output_trained_modelAndDicts + "-" + str(
        hyper_parameters.hyper_parameter["batch_size"]) + "_" + str(
        hyper_parameters.hyper_parameter["number_epochs"]) + "_" + str(
        hyper_parameters.hyper_parameter["number_hidden_lstm_layers"]) + "_" + str(
        hyper_parameters.hyper_parameter["lstm_units"]) + "_" + str(hyper_parameters.hyper_parameter["max_length"])
    if not os.path.exists(model_trained_directory):
        os.mkdir(model_trained_directory)
    model_path = os.path.join(model_trained_directory, "model.h5")
    callbacks = [
        ReduceLROnPlateau(verbose=1),
        EarlyStopping(patience=100, verbose=1),
        keras.callbacks.ModelCheckpoint(filepath=model_path, monitor='val_loss', save_best_only=True)
    ]

    history_train = model.fit(X_train, [Y_train],
                              batch_size=hyper_parameters.hyper_parameter["batch_size"],
                              epochs=hyper_parameters.hyper_parameter["number_epochs"],
                              validation_split=0.3,
                              class_weight="auto",
                              callbacks=callbacks,
                              use_multiprocessing=True
                              )
    with open(os.path.join(model_trained_directory, "word2id.pck"), "wb") as o:
        pickle.dump(word2id, o)
    with open(os.path.join(model_trained_directory, "id2label.pck"), "wb") as o:
        pickle.dump(id2label, o)
    with open(os.path.join(model_trained_directory, "history.history"), "wb") as o:
        pickle.dump(history_train, o)


if __name__ == "__main__":
    app.run(main)
