# Procedure

    Train a Q&A model based on pre-trained a English BERT model with squad training data
    Predict some answers with the trained model
    Train a Q&A model based on a pre-trained German model


# General

To train the model this repository has been used:

https://github.com/kamalkraj/BERT-SQuAD

The training data is the SQuAD training data set.A

## How to use the model





# English Q&A Model

## Commando used

## Model parameters

## Results

EN - EN 3 Epochs
 "architectures": [
    "BertForMaskedLM"
  ],
  "attention_probs_dropout_prob": 0.1,
  "finetuning_task": null,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 1024,
  "initializer_range": 0.02,
  "intermediate_size": 4096,
  "layer_norm_eps": 1e-12,
  "max_position_embeddings": 512,
  "num_attention_heads": 16,
  "num_hidden_layers": 24,
  "num_labels": 2,
  "output_attentions": false,
  "output_hidden_states": false,
  "pruned_heads": {},
  "torchscript": false,
  "type_vocab_size": 2,
  "vocab_size": 30522
}



{
  "exact": 51.5042573320719,
  "f1": 65.54151208821962,
  "total": 10570,
  "HasAns_exact": 51.5042573320719,
  "HasAns_f1": 65.54151208821962,
  "HasAns_total": 10570
}

``
python -m torch.distributed.launch --nproc_per_node=1 run_squad.py    --model_type bert     --model_name_or_path bert-large-uncased-whole-word-masking     --do_train     --do_eval          --train_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/train-v1.1.json     --predict_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/dev-v1.1.json     --learning_rate 3e-5     --num_train_epochs 3     --max_seq_length 384     --doc_stride 128      --output_dir ../models/english_squad/     --per_gpu_eval_batch_size=2       --per_gpu_train_batch_size=2 --overwrite_output_dir --verbose_logging
``

EN - EN 2 Epochs

 "architectures": [
    "BertForMaskedLM"
  ],
  "attention_probs_dropout_prob": 0.1,
  "finetuning_task": null,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 1024,
  "initializer_range": 0.02,
  "intermediate_size": 4096,
  "layer_norm_eps": 1e-12,
  "max_position_embeddings": 512,
  "num_attention_heads": 16,
  "num_hidden_layers": 24,
  "num_labels": 2,
  "output_attentions": false,
  "output_hidden_states": false,
  "pruned_heads": {},
  "torchscript": false,
  "type_vocab_size": 2,
  "vocab_size": 30522
}
