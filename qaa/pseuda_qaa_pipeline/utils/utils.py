import csv

item_ids_index = 0


def write_list2csv(list2write: list, output_file: str):
    """
    Write list into a csv file
    :param list2write:
    :param output_file:
    """
    with open(output_file, "w", encoding="utf-8") as o:
        csv_writer = csv.writer(o)
        for element in list2write:
            csv_writer.writerow(element)


def get_items_ids(input_file: str) -> list:
    """
    extract the item_ids and return a list
    :param input_file:
    :return: List of item_ids
    """
    list_item_ids: list = []
    with open(input_file, "r", encoding="utf-8") as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            list_item_ids.append(row[item_ids_index])
    return list_item_ids