input_output_files: dict = \
    {"prediction":
        {
            "model_path": "/home/aurelien/br_repositories/machine_learning_experiments/bert_squad/models/en_2_epochs",
            "questions_path": "/home/aurelien/br_repositories/machine_learning_experiments/bert_squad/predictions/questions/construction_en_address_2.txt",
            "input_texts": "/home/aurelien/br_repositories/machine_learning_experiments/bert_squad/data/test_set_qqa_40.csv",
            "output_predictions": "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data/result_predictions.csv"
        },
        "post_processing":
            {
                "output_predictions": "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data/result_predictions.csv",
                "input_csv_post_processing": "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data/result_predictions.csv ",
                "output_post_processing": "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data/predictions_post_processed.csv"
            }

    }

headers_pipe = ["item_id", "text", "pipe_address_city", "pipe_address_road", "pipe_gfa", "pipe_volume_string"]