"""

Q&A'S pseudo pipeline for testing
    First we pre process the text in order to put into the required model's format
    After we get the predictions. We predict with several questions for one given text
    Post processing of the predictions/answers. Some predictions may not be at all what we want (e.g. "in 2 hours"
        is not an address)
    With the input(s) from our production pipeline plus our predictions, we build a string to be processed by a
    normalizer
    Finally we get the normalize string

"""


if __name__ == '__main__':
    # pre_processor
    print("Pre Processing: Begin")
    pre_processor = PreProcessor(input_strings=["prediction1", "prediction2"])
    pre_processed_string = pre_processor.normalize_string()
    print("Pre Processing: Done")

    # predicting
    print("Predicting: Begin")
    predicting = False
    if predicting:
        predictor: Predictor = Predictor(model_path=input_output_files["prediction"]["model_path"],
                                         texts_path=input_output_files["prediction"]["input_texts"],
                                         questions_path=input_output_files["prediction"]["questions_path"])
        headers: list = headers_pipe + list(predictor.dict_questions.keys())
        predictions: list = [headers] + predictor.do_predictions_evaluate()
        write_list2csv(list2write=predictions, output_file=input_output_files["prediction"]["output_predictions"])
    print("Predicting: Done")

    print("Pos Processing: Begin")
    # post processing
    output_predictions: str = "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data" \
                              "/result_predictions.csv"
    input_csv_post_processing: str = "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data" \
                                     "/result_predictions.csv "
    output_post_processing: str = "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data" \
                                  "/predictions_post_processed.csv"

    post_processor: PostProcessor = PostProcessor(input_csv=input_csv_post_processing, domain="street")
    predictions_resolved = post_processor.resolve_predictions()
    write_list2csv(predictions_resolved, output_post_processing)
    print("Pos Processing: Done")

    # quering
    print("Sending Queries: Begin")
    querier: Querier = Querier(post_processor=post_processor, results_predictions_path=input_csv_post_processing,
                               pipe_info_index=2)
    list_addresses: list = querier.send_queries()
    print("Sending Queries: Done")

    # Write to file
    print("Writing to file: Begin")
    list_items_id: list = get_items_ids(input_file=input_csv_post_processing)
    for item_id,answer in zip(list_items_id,list_addresses):
        print(list(answer.values()),list(answer.keys()))
    print("Writing to file: Done")

