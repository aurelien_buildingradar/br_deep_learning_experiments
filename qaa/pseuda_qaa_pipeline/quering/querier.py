import csv
import requests

from qaa.pseuda_qaa_pipeline.post_processing import PostProcessor


class Querier:
    def __init__(self, post_processor: PostProcessor, results_predictions_path: str, pipe_info_index: int):
        self.post_processor = post_processor
        self.url = "http://10.1.1.5:80/search.php?"
        self.results_predictions_path = results_predictions_path
        self.pipe_info_index = pipe_info_index
        self.strings_to_add = self.get_list_string_to_add()
        self.list_queries = self.create_concatened_strings()

    def get_list_string_to_add(self) -> list:
        strings: list = []
        with open(self.results_predictions_path, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                strings.append(row[self.pipe_info_index])
        return strings

    def create_concatened_strings(self) -> list:
        list_queries: list = []
        for added_string, prediction in zip(self.strings_to_add, self.post_processor.resolved_predictions):
            list_queries.append(added_string + " " + prediction)
        return list_queries

    def send_queries(self) -> list:

        list_address: list = []
        for query in self.list_queries:
            if "NO_ADDRESS_FOUND" in query:
                query = query.split(" ")
                query = " ".join(query[0:-1])

            answer: dict = {"query_geo_service": query,
                            "output_geo_service_road": "NO_ADDRESS_EXTRACTED",
                            "output_geo_service_city": "NO_ADDRESS_EXTRACTED",
                            "output_geo_service_state": "NO_ADDRESS_EXTRACTED",
                            "output_geo_service_country": "NO_ADDRESS_EXTRACTED"
                            }
            if True:

                parameters: dict = {"accept - language": "en",
                                    "format": "json",
                                    "namedetails": 1,
                                    "addressdetails": 1,
                                    "q": query,
                                    "limit": 2,
                                    "extratags": 1}

                r = requests.get(self.url, parameters).json()
                if len(r) == 0:
                    list_address.append(answer)
                else:
                    road: str = r[0]["address"].get("road", "NO_ADDRESS_EXTRACTED")
                    city: str = r[0]["address"].get("town", "NO_ADDRESS_EXTRACTED")
                    state: str = r[0]["address"].get("state", "NO_ADDRESS_EXTRACTED")
                    country: str = r[0]["address"].get("country", "NO_ADDRESS_EXTRACTED")

                    answer: dict = {"query_geo_service": query,
                                    "output_geo_service_road": road,
                                    "output_geo_service_city": city,
                                    "output_geo_service_state": state,
                                    "output_geo_service_country": country
                                    }

                    list_address.append(answer)
        return list_address
