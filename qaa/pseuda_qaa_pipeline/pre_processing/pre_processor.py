class PreProcessor:
    def __init__(self, input_strings: list):
        """
        Allow to pre process string
        :param input_strings: List of the input strings
        """
        self.input_strings = input_strings
        self.pre_processed_strings = self.normalize_string()

    def normalize_string(self) -> list:
        """
        :return: The pre processed string
        """

        pre_processed_string: list = []
        for string in self.input_strings:
            # todo implement pre processing
            pre_processed_string.append(string)
        return pre_processed_string
