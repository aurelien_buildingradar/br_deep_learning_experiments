import csv

import tqdm

from qaa.pseuda_qaa_pipeline.predicting import QA


class Predictor:
    """
    Based on model and questions predicts the anwsers
    """

    def __init__(self, model_path: str, texts_path, questions_path):
        """
        :param model_path: Path to all the files in order to predict
        :param texts_path:
        :param questions_path:
        """

        self.model: QA = QA(model_path)
        self.dict_texts: dict = self.get_texts_evaluate(texts_path, delimiter="\t", position_item_id=0, position_text=1)
        self.dict_questions: dict = self.get_questions(questions_path)

    @staticmethod
    def pred(model: QA, text: str, question: str):
        """
         Arguments:
                model: Model used for the prediction
                text: String on which one the prediction will be done
                question: Question used to do the prediction
         Return:
             prediction: Answer/prediction

        """
        return model.predict(text, question)

    @staticmethod
    def get_questions(input_file: str) -> dict:
        """
             Arguments:
                    input_file:
             Return:
                 dict_questions: question_goal:question

        """
        dict_questions: dict = {}
        with open(input_file, "r", encoding="utf-8") as f:
            for line in f:
                line = line.replace("\n", "")
                goal, question = line.split(";")
                dict_questions[question] = goal
        return dict_questions

    @staticmethod
    def get_texts_evaluate(csv_input: str, delimiter: str, position_item_id: int, position_text: int) -> dict:
        """
         Arguments:
                csv_input:
                delimiter:
                position_text:
                position_item_id:
         Return:
             dictionary_item_ids_texts:
        """
        dictionary_item_ids_texts: dict = {}
        with open(csv_input, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f, delimiter=delimiter)
            next(csv_reader, None)
            for row in csv_reader:
                dictionary_item_ids_texts[row[position_item_id]] = {"text": row[position_text],
                                                                    "pipe_address.city": row[2],
                                                                    "pipe_address.road": row[3],
                                                                    "pipe_gfa": row[4],
                                                                    "pipe_volume_string": row[5]}
        return dictionary_item_ids_texts

    def do_predictions_evaluate(self):
        """
        For loop to do the prediction
        todo think about a better and general structure see    pipe_address_city = data["pipe_address.city"]
        :return: results[item_id] = {"text": text, "results": answers}
        """
        results: list = []
        for item_id, data in tqdm.tqdm(self.dict_texts.items()):
            text = data["text"]
            pipe_address_city = data["pipe_address.city"]
            pipe_address_road = data["pipe_address.road"]
            pipe_gfa = data["pipe_gfa"]
            pipe_volume_string = data["pipe_volume_string"]
            answers: list = []
            for question, goal in self.dict_questions.items():
                prediction: dict = self.pred(self.model, text, question)
                answer: str = prediction["answer"]
                confidence: str = prediction["confidence"]
                answer = answer + " (" + str(float(round(confidence, 2))) + ")"
                answers.append(answer)
            results.append(
                [item_id, text, pipe_address_city, pipe_address_road, pipe_gfa, pipe_volume_string] + answers)

        return results


