import re


class Prediction:
    def __init__(self, predictions: list, domain):
        self.predictions = predictions
        self.domain = domain
        self.resolved_prediction: str = self.resolve_prediction()

    def resolve_prediction(self) -> str:
        """
        Because we have more than one predictions for on item we need to find out which one
        may be correct
        :return: the assumed correct prediction
        """
        correct_prediction: str = self.domain_resolver()
        return correct_prediction

    def domain_resolver(self):
        if self.domain == "street" or "address":
            return self.resolver_address()

    def resolver_address(self) -> str:
        interesting_items = self.predictions[6:]
        addresses_found: list = []

        street_address_regex = r'\d{1,10}( \w+){1,10}( ( \w+){1,10})?( \w+){1,10}[,.](( \w+){1,10}(,)? [A-Z]{2}( [' \
                               r'0-9]{5})?)? '
        for prediction in interesting_items:
            matches = re.finditer(street_address_regex, prediction, re.MULTILINE)
            for matchNum, match in enumerate(matches, start=1):
                addresses_found.append(match.group())
        add_freq: dict = {}
        for add_found in addresses_found:
            if add_found not in add_freq.keys():
                add_freq[add_found] = 1

            else:
                add_freq[add_found] += 1

        best_address = self.get_prediction_with_high_occurences(add_freq)
        best_address = self.clean_string(best_address)
        return best_address

    @staticmethod
    def clean_string(string: str) -> str:
        string = string.replace(",", " ")
        return string

    def get_prediction_with_high_occurences(self, dict_occ: dict) -> str:
        sorted_dict_add_oc: dict = {k: v for k, v in reversed(sorted(dict_occ.items(), key=lambda item: item[1]))}
        occ_addr = {v: k for k, v in sorted_dict_add_oc.items()}
        if len(occ_addr.items()) > 0:
            return occ_addr[list(occ_addr.keys())[0]]

        else:
            return "NO_ADDRESS_FOUND"
