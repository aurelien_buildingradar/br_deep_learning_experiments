import csv
from qaa.pseuda_qaa_pipeline.prediction import Prediction


class PostProcessor:
    """
    Based on an input, with this class you can post process the predictions.

    """

    def __init__(self, input_csv: str, domain: str):
        """
        :param input_csv: file containing the predictions
        :param domain: What is the question about (e.g. street,volume etc.)
        """
        self.input_csv: str = input_csv
        self.headers, self.predictions = self.read_prediction()
        self.domain: str = domain
        self.resolved_predictions:list = self.resolve_predictions()

    def read_prediction(self) -> tuple:
        """
        Get the inputs to be pre processed
        :return: The headers and the predictions will be returned as a tuple
        """
        predictions: list = []
        with open(self.input_csv, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                predictions.append(row)
        headers = predictions[0]
        predictions = predictions[1:]
        return headers, predictions

    def resolve_predictions(self) -> list:
        """
        Resolve the predictions, because there are multiple possible answers, we need to know
        which one is correct.
        Create a Prediction object in order to do it.
        :return: List of predictions (1 dimension list)
        """
        resolved_predictions: list = []
        for prd in self.predictions:
            pred: Prediction = Prediction(prd, self.domain)
            resolved_predictions.append(pred.resolved_prediction)
        return resolved_predictions
