from datetime import datetime

from bert import QA

"""
Mini script to test the Q&A model.
Be aware to have the correct bert_config.json file

"""
# doc = "Victoria has a written constitution enacted in 1975, but based on the 1855 colonial constitution, passed by the United Kingdom Parliament as the Victoria Constitution Act 1855, which establishes the Parliament as the state's law-making body for matters coming under state responsibility. The Victorian Constitution can be amended by the Parliament of Victoria, except for certain 'entrenched' provisions that require either an absolute majority in both houses, a three-fifths majority in both houses, or the approval of the Victorian people in a referendum, depending on the provision

model = QA('/home/aurelien/repositories/ml_tests/transformers/BERT-SQuAD/model/wwm_uncased_finetuned_squad')

docs = [
    "LONDON (Reuters) - Britain will build a further two temporary hospitals to treat coronavirus patients, the National Health Service (NHS) said on Friday as its first field hospital prepares to open in London., Britain is bracing itself for the peak of a virus outbreak which has claimed nearly 3,000 lives domestically and more than 48,000 around the world., The NHS said it would build a 1,000-patient facility at a university in Bristol, south-west England, and a 500-bed facility at a conference centre in Harrogate in the north of the country., That means it is now planning to open five field hospitals in the coming weeks. The first, the NHS Nightingale in east London, is due to be officially opened on Friday, and will receive its first patients next week., Named after nursing pioneer Florence Nightingale who tended to British soldiers in the Crimean War, the hospital has been built inside a cavernous conference centre in just two weeks, with help from the military., Other temporary hospitals in Manchester and Birmingham will provide up to 3,000 beds in total., (This story removes extraneous word in headline), Reporting by William James, editing by Estelle Shirbon",
    "Work is wrapping up on The Symon, a 13-story condominium building at 70 Schermerhorn Street in Brooklyn Heights. Designed by Eugene Colberg and developed by Lonicera Partners and Orange Management, the project is located between Boerum Place and Court Street and is steps away from the faux subway entrance to the subterranean New York Transit Museum., Photos from street level show the façade nearly finished, with the exception of the roof levels and the ground floor behind the sidewalk scaffolding. The warm brick curtain wall is fully installed and the windows are almost all in place., The 86,000-square-foot edifice will yield 68,460 square feet of residential space and 3,000 square feet of retail space on the ground floor facing Schermerhorn Street. There will be a total of 59 residential units, averaging 1,163 square feet apiece. Units range from studios to four-bedroom spreads designed by Studio DB. Amenities include a 24-hour attended lobby, a private lounge, a landscaped rooftop terrace, a gym with Peloton fitness equipment, a children's playroom, storage, bicycle spaces for purchase, and a limited number of indoor automatic parking spots, also available for purchase., The closest subways to the property are the 2, 3, 4, and 5 trains at Borough Hall and the A, C, F, and R trains at Jay Street-MetroTech., 70 Schermerhorn should likely finish before the end of the year., Subscribe to YIMBY's daily e-mail, Follow the YIMBYgram for real-time photo updates, Like YIMBY on Facebook, Follow YIMBY's Twitter for the latest in YIMBYnews",
    "New York-based Ultimate Realty launched the $12-million redevelopment of the former Westgate Shopping Center in Macon, GA. The 395,000-square-foot, six-building industrial complex will be rebranded as Middle Georgia Industrial Park., Ultimate Realty s Joe Sabbagh says, The former Westgate Shopping Mall is in a prime setting for a retail-to-industrial conversion, with the new name referring to the optimal location Macon has become for the state of Georgia. Our unique vision will bring a world-class redevelopment to the market, resulting in the addition of hundreds of jobs in Macon., Interior rehab work will include new electrical systems, bathrooms, new loading docks and drive- in doors, office pod additions and repainting of the facades. The renovation plan will also include upgrading the signage, major landscaping work, new LED lighting, upgraded sprinklers, and expansive truck parking. Work is scheduled to be completed this summer., Cushman & Wakefield s Courtney Oldenburg and Chris Copenhaver are overseeing leasing.",
    "Missouri highway officials approved a plan to complete their remaining portion of the Bella Vista Bypass, also known as the Interstate 49 Missouri/Arkansas Connector., The Missouri Highways and Transportation Commission on Wednesday awarded a project to build the final 5 miles of I-49 between Pineville and the Missouri/Arkansas line in McDonald County, according to a news release from the Missouri Department of Transportation., The project was awarded to Emery Sapp & Sons of Columbia, Mo., for $58.7 million. Construction is expected to begin in late April or early May with completion by Sept. 30, 2021. The road will be a four-lane, limited access highway built to interstate standards. An interchange will be built at Missouri Route 90 west of Jane., The Northwest Arkansas Regional Planning Commission got a $25 million federal grant in December 2018 and gave the money to Missouri to complete I-49. The goal is for Arkansas and Missouri to each build their parts and meet at the state line in late 2021, or early 2022 at the latest, according to regional planners. The 19-mile connector will allow motorists to circumvent Bella Vista to the west and south on a four-lane interstate., The project has been discussed for more than 25 years and is considered a priority by Northwest Arkansas and federal transportation officials. The Northwest Arkansas Council, a group of civic and business leaders, prioritized completion of the highway, describing it as one of the region's most important projects., A lack of money for the remaining section had been delaying completion of the 278-mile section of interstate between Fort Smith and Kansas City, Mo. Bella Vista is the only stretch where traffic has to leave I-49 and continue on U.S. 71 to travel north or south. Allowing motorists to bypass Bella Vista, and its multiple traffic signals, should reduce travel times and improve safety, planners have said., Arkansas officials broke ground in October on two sections needed to fill missing links on the Arkansas side., The two projects are the last 2.5 miles from Hiwasse to the state line and a single-point urban interchange to replace the roundabout at I-49 and U.S. 71 in north Bentonville where the new highway heads west. The estimated cost of the projects is just more than $100 million., Metro on 04/03/2020 Print Headline: Missouri to finish its final I-49 link Sponsor Content"]

questions = ["Where is the building being built ?",
             "What is being built ? ",
             "When is the construction starting ? ",
             "What is the cost of the building?", "What is the area being built?",

                                                  "Where is the  project built ?",
             "What is being project ? ",
             "When is the construction starting ? ", "What is the cost of the project?",
             "What is the area being built?"
             ]

for d in docs:
    for q in questions:
        b = datetime.now()
        answer = model.predict(d, q)
        e = datetime.now()
        print("doc :", answer["document"][:50])
        print("question :", q)
        print("answer: ", answer['answer'])

        # print("Prediction took :",e-b)
        # print(answer.keys())
        print("+" * 50)
    print("*" * 100)
