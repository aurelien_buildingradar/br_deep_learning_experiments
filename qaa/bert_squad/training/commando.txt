python -m torch.distributed.launch --nproc_per_node=1 run_squad.py    --model_type bert     --model_name_or_path bert-large-uncased-whole-word-masking   -do_lower_case  --do_train     --do_eval     --do_lower_case     --train_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/train-v1.1.json     --predict_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/dev-v1.1.json     --learning_rate 3e-5     --num_train_epochs 2     --max_seq_length 384     --doc_stride 128      --output_dir ../models/english_squad_2_epochs/     --per_gpu_eval_batch_size=3       --per_gpu_train_batch_size=3 --overwrite_output_dir --verbose_logging


python -m torch.distributed.launch --nproc_per_node=1 run_squad.py    --model_type bert     --model_name_or_path bert-base-german-cased     --do_train     --do_eval     --do_lower_case     --train_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/dev-v1.1.json     --predict_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/dev-v1.1.json     --learning_rate 3e-5     --num_train_epochs 1     --max_seq_length 384     --doc_stride 128      --output_dir ../models/wwm_uncased_finetuned_squad/     --per_gpu_eval_batch_size=2       --per_gpu_train_batch_size=2 --overwrite_output_dir


## de-de

python -m torch.distributed.launch --nproc_per_node=1 run_squad.py    --model_type bert     --model_name_or_path bert-base-german-cased     --do_train     --do_eval     --train_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/german_dev.json     --predict_file /home/azureuser/cloudfiles/code/Users/a.levecq/test_seq2seq/BERT-SQuAD/data/german_test.json     --learning_rate 3e-5     --num_train_epochs 100     --max_seq_length 384     --doc_stride 128      --output_dir ../models/german_fine_tuned_squad/     --per_gpu_eval_batch_size=12      --per_gpu_train_batch_size=12 --overwrite_output_dir --verbose_logging --save_steps 250


