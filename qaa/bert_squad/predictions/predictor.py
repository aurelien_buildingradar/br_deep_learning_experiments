import argparse
import csv
import pickle
from collections import defaultdict
from datetime import datetime
import itertools

from bert import QA

"""
This script aims to predicting based on a given text and given question(s)

Mini script to test the Q&A model.
Be aware to have the correct bert_config.json file
"""


class Predictor:

    def __init__(self, model_path, texts_path, questions_path):

        self.model: QA = QA(model_path)
        self.dict_texts: dict = self.get_texts_evaluate(texts_path, delimiter="\t", position_item_id=0, position_text=1)
        self.dict_questions: dict = self.get_questions(questions_path)

    def pred(self, model: QA, text: str, question: str):
        """
         Arguments:
                model: Model used for the prediction
                text: String on which one the prediction will be done
                question: Question used to do the prediction
         Return:
             prediction: Answer/prediction

        """
        return model.predict(text, question)

    def get_questions(self, input_file: str) -> dict:
        """
             Arguments:
                    input_file:
             Return:
                 dict_questions: question_goal:question

        """
        dict_questions: dict = {}
        with open(input_file, "r", encoding="utf-8") as f:
            for line in f:
                line = line.replace("\n","")
                goal, question = line.split(";")
                dict_questions[question] = goal
        return dict_questions

    def get_texts_evaluate(self, csv_input: str, delimiter: str, position_item_id: int, position_text: int) -> dict:
        """
         Arguments:
                csv_input:
                delimiter:
                position_text:
                position_item_id:
         Return:
             dictionary_item_ids_texts:
        """
        dictionary_item_ids_texts: dict = {}
        with open(csv_input, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f, delimiter=delimiter)
            next(csv_reader, None)
            for row in csv_reader:
                dictionary_item_ids_texts[row[position_item_id]] = {"text": row[position_text],
                                                                    "pipe_address.city": row[2],
                                                                    "pipe_address.road": row[3],
                                                                    "pipe_gfa": row[4],
                                                                    "pipe_volume_string": row[5]}
        return dictionary_item_ids_texts

    def do_predictions_evaluate(self):
        results: list = []
        i = 0
        max_item_to_process = 41
        for item_id, data in self.dict_texts.items():
            text = data["text"]
            pipe_address_city = data["pipe_address.city"]
            pipe_address_road = data["pipe_address.road"]
            pipe_gfa = data["pipe_gfa"]
            pipe_volume_string = data["pipe_volume_string"]
            if i < max_item_to_process:
                print(i, max_item_to_process)
                answers: list = []
                for question, goal in self.dict_questions.items():
                    prediction: dict = self.pred(self.model, text, question)
                    if goal == "street":
                        pass
                    if goal == "city":
                        pass

                    answer: str = prediction["answer"]
                    confidence: str = prediction["confidence"]
                    answer = answer + " (" + str(float(round(confidence, 2))) + ")"
                    answers.append(answer)
                print([item_id, text, pipe_address_city, pipe_address_road, pipe_gfa, pipe_volume_string] + answers)
                results.append(
                    [item_id, text, pipe_address_city, pipe_address_road, pipe_gfa, pipe_volume_string] + answers)

                # results[item_id] = {"text": text, "results": answers}
                # print(results[item_id])
                i += 1
        return results


def main():
    parser = argparse.ArgumentParser()

    ## Required parameters
    parser.add_argument("--model_directory_path", default=None, type=str, required=True,
                        help="Directory where the model and its dependencies is stored")
    parser.add_argument("--questions_file", default=None, type=str, required=True,
                        help="File containing the list of questions")
    parser.add_argument("--texts_file", default=None, type=str, required=True,
                        help="File containing the list of texts to be predicted")

    args = parser.parse_args()

    if 1 == 1:
        predictor = Predictor(args.model_directory_path, args.texts_file, args.questions_file)
        headers = ["item_id", "text", "pipe_address_city", "pipe_address_road", "pipe_gfa", "pipe_volume_string"] \
                  + list(predictor.dict_questions.keys())
        results = [headers] + predictor.do_predictions_evaluate()

        with open("result_predictions.csv", "w") as o:
            csv_writer = csv.writer(o)
            for r in results:
                csv_writer.writerow(r)
        with open("results.pck", "wb") as o:
            pickle.dump(results, o)
    if 1 == 2:
        with open("/home/aurelien/repositories/machine_learning_experiments/bert_squad/predictions/results.pck",
                  "rb") as i:
            r = pickle.load(i)
    for item_id, pred in r.items():
        print("+" * 100)
        print(pred["results"])


if __name__ == "__main__":
    main()
