#Commando

``
python3 /home/aurelien/repositories/machine_learning_experiments/bert_squad/predictions/predictor.py --model_directory_path /home/aurelien/repositories/ml_tests/transformers/BERT-SQuAD/model/english_squad_2_epochs --questions_file /home/aurelien/repositories/machine_learning_experiments/bert_squad/predictions/questions/construction_en.txt --texts_file /home/aurelien/repositories/machine_learning_experiments/bert_squad/data/test_set_qqa.csv
``