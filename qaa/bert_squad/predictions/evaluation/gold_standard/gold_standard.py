import csv


class GoldStandard:
    def __init__(self,gold_standard_file: str):

        self.goldstandard_file: str = gold_standard_file
        self.goldstandard: dict = self.read_gold_standard(file=self.goldstandard_file,delimiter="\t")

    @staticmethod
    def read_gold_standard(file: str, delimiter: str) -> dict:
        """
        Read the gold standard file and return a dictionary:
            {"item_id":
                {"domain":"correct_string"}...
            }
        """
        data: dict = {}
        with open(file, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f, delimiter=delimiter)
            next(csv_reader)
            for row in csv_reader:
                item_id: str = row[0]
                street: str = row[2]
                city: str = row[3]
                volume: str = row[4]
                gfa: str = row[5]

                data[item_id] = {
                    "street": street,
                    "city": city,
                    "volume": volume,
                    "gfa": gfa
                }
        return data