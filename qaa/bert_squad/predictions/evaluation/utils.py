import csv


def get_headers2id(file: str, delimiter: str) -> dict:
    with open(file, "r", encoding="utf-8") as f:
        csv_reader = csv.reader(f, delimiter=delimiter)
        for row in csv_reader:
            mapping: dict = {columnName: index for index, columnName in enumerate(row)}
            break
    return mapping


def reverse_dict(d: dict) -> dict:
    """
    Reverse key value in a dict
    """
    return {v: k for k, v in d.items()}

def sort_dict_values(d:dict,order)-> dict:
    if order =="asc":
        return {k: v for k, v in sorted(d.items(), key=lambda item: item[1])}
    if order == "desc":
        return {k: v for k, v in reversed(sorted(d.items(), key=lambda item: item[1]))}

