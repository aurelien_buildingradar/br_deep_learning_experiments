from difflib import SequenceMatcher


class DistanceComputer:
    """
    def __init__(self, list_string1: list, list_string2: list):
        self.list_string1: list = list_string1
        self.list_string2: list = list_string2
    """

    @staticmethod
    def jaro(s, t):
        """Jaro distance between two strings."""
        s_len = len(s)
        t_len = len(t)

        if s_len == 0 and t_len == 0:
            return 1

        match_distance = (max(s_len, t_len) // 2) - 1

        s_matches = [False] * s_len
        t_matches = [False] * t_len

        matches = 0
        transpositions = 0

        for i in range(s_len):
            start = max(0, i - match_distance)
            end = min(i + match_distance + 1, t_len)

            for j in range(start, end):
                if t_matches[j]:
                    continue
                if s[i] != t[j]:
                    continue
                s_matches[i] = True
                t_matches[j] = True
                matches += 1
                break

        if matches == 0:
            return 0

        k = 0
        for i in range(s_len):
            if not s_matches[i]:
                continue
            while not t_matches[k]:
                k += 1
            if s[i] != t[k]:
                transpositions += 1
            k += 1

        return ((matches / s_len) +
                (matches / t_len) +
                ((matches - transpositions / 2) / matches)) / 3

    @staticmethod
    def compute_jaro_distance(self, string1: str, string2: str) -> float:
        return self.jaro(string1, string2)

    def get_distances(self, list_string1: list, list_string2: list, algorithm: str) -> list:
        results: list = []
        for s1, s2 in zip(list_string1, list_string2):
            if algorithm == "jaro":
                results.append(self.jaro(s1, s2))
            if algorithm == "sequence_matcher":
                results.append(self.compute_sequence_matching(s1, s2))

        return results

    @staticmethod
    def compute_average(list_distance: list) -> float:
        return round(sum(list_distance) / len(list_distance), 4)

    @staticmethod
    def compute_sequence_matching(s1: str, s2: str) -> float:
        return SequenceMatcher(None, s1, s2).ratio()
