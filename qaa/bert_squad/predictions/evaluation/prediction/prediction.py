import csv
from collections import defaultdict

from qaa.bert_squad.predictions.evaluation.prediction import domains_mapping
from qaa.bert_squad.predictions import get_headers2id, reverse_dict


class Prediction:
    def __init__(self, prediction_file: str):
        self.prediction_file: str = prediction_file
        self.predictions: dict = self.read_predictions(file=prediction_file, delimiter=",")

    def get_question(self, index: int) -> str:
        headers2id: dict = get_headers2id(self.prediction_file, ",")
        id2headers: dict = reverse_dict(headers2id)
        return id2headers[index]

    def read_predictions(self, file: str, delimiter: str) -> dict:
        """
        {"item_id":
            {"question":["domain","answer"],"question2":["domain","answer2"]}
        }
        """
        data: dict = defaultdict(dict)
        with open(file, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f, delimiter=delimiter)
            next(csv_reader)

            question2id: dict = get_headers2id(self.prediction_file, delimiter)
            id2question: dict = reverse_dict(question2id)
            for row in csv_reader:
                item_id = row[0]
                content: dict = {}
                for i in range(0, len(row)):  # Questions are starting at index 6
                    row[i] = row[i][:-7]  # remove conf score
                    content[id2question[i]] = [domains_mapping[id2question[i]], row[i]]

                data[item_id] = content
        return data
