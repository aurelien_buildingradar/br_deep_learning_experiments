from collections import defaultdict

from qaa.bert_squad.predictions.evaluation.distance_computor.distance_computor import DistanceComputer
from qaa.bert_squad.predictions import GoldStandard
from qaa.bert_squad.predictions.evaluation.prediction import Prediction
from qaa.bert_squad.predictions import sort_dict_values

"""
This script computes the score of different questions (versus gold standard)
"""


def main():
    """
    Compute for each domain and for each question the average jaro distance
    """
    distance_computer: DistanceComputer = DistanceComputer()
    gold_standard_file = "/qaa/bert_squad/predictions/data/gold_standard.tsv"
    gold_standard: GoldStandard = GoldStandard(gold_standard_file)
    domains: list = ["street","address"]
    distances: dict = {}
    prediction_file = "/qaa/bert_squad/predictions/data/predictions.csv"
    prediction = Prediction(prediction_file=prediction_file)

    results: dict = defaultdict(list)
    for domain in domains:
        for item_id, gs_value in gold_standard.goldstandard.items():
            for question, domain_answer in prediction.predictions[item_id].items():
                pred_domain, pred_answer = domain_answer
                if domain == pred_domain:
                    # "question":["gs","pred"]
                    results[question].append([gs_value[domain], pred_answer])

    distances: dict = {}
    for question, gs_pred in results.items():
        list_gs: list = [gs for gs, pred in gs_pred]
        list_pred: list = [pred for gs, pred in gs_pred]
        list_distances = distance_computer.get_distances(list_gs, list_pred, algorithm="sequence_matcher")

        average_distance = distance_computer.compute_average(list_distances)
        distances[question] = average_distance
        distances = sort_dict_values(distances, "desc")

    for key, value in distances.items():
        print(key, value)

    print(distances["What is the address of the project ?"])


main()
