"""

Q&A'S pseudo pipeline for testing
    First we pre process the text in order to put into the required model's format
    After we get the predictions. We predict with several questions for one given text
    Post processing of the predictions/answers. Some predictions may not be at all what we want (e.g. "in 2 hours"
        is not an address
    With the input(s) from our production pipeline plus our predictions, we build a string to be processed by our
    normalizer
    Finally we get the normalize string

"""
from pseuda_qaa_pipeline.post_processing.post_processor import PostProcessor
from pseuda_qaa_pipeline.pre_processing.pre_processor import PreProcessor
from pseuda_qaa_pipeline.predicting.predictor import Predictor
from pseuda_qaa_pipeline.quering.querier import Querier

if __name__ == '__main__':
    # pre_processor
    pre_processor = PreProcessor()

    # predicting
    predicting = False
    if predicting:
        model_path: str = "/home/aurelien/br_repositories/machine_learning_experiments/bert_squad/models/en_2_epochs"
        questions_path: str = "/home/aurelien/br_repositories/machine_learning_experiments/bert_squad/predictions" \
                              "/questions/construction_en_address_2.txt"
        input_texts: str = "/home/aurelien/br_repositories/machine_learning_experiments/bert_squad/data/test_set_qqa_40.csv"
        output_predictions: str = "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data" \
                                  "/result_predictions.csv "
        predictor: Predictor = Predictor(model_path=model_path, texts_path=input_texts, questions_path=questions_path,
                                         output_predictions=output_predictions)
        headers: list = ["item_id", "text", "pipe_address_city", "pipe_address_road", "pipe_gfa", "pipe_volume_string"] \
                        + list(predictor.dict_questions.keys())
        predictions: list = [headers] + predictor.do_predictions_evaluate()
        predictor.write_prediction(predictions)

    # post processing
    output_predictions: str = "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data" \
                              "/result_predictions.csv "
    input_csv_post_processing: str = output_predictions
    output_post_processing: str = "/home/aurelien/br_repositories/machine_learning_experiments/pseuda_qaa_pipeline/data" \
                                  "/predictions_post_processed.csv "

    post_processor: PostProcessor = PostProcessor(input_csv=input_csv_post_processing,
                                                  output_post_processing=output_post_processing,domain="street")
    predictions_resolved = post_processor.resolve_predictions()

    # quering
    querier: Querier = Querier(post_processor=post_processor,results_predictions_path=output_predictions,pipe_info_index=2)
    list_addresses:list = querier.send_queries()

    # write to csv

    for answer in list_addresses:
        print(answer)