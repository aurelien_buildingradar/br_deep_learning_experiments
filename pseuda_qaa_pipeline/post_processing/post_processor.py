import csv

from pseuda_qaa_pipeline.prediction.prediction import Prediction


class PostProcessor:
    def __init__(self, input_csv: str, output_post_processing: str, domain):
        self.input_csv: str = input_csv
        self.output_post_processing: str = output_post_processing
        self.headers, self.predictions = self.read_prediction()
        self.domain = domain
        self.resolved_predictions = self.resolve_predictions()

    def read_prediction(self):
        predictions: list = []
        with open(self.input_csv, "r", encoding="utf-8") as f:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                predictions.append(row)
        headers = predictions[0]
        predictions = predictions[1:]
        return headers, predictions

    def resolve_predictions(self):
        resolved_predictions: list = []
        for prd in self.predictions:
            pred = Prediction(prd, self.domain)
            resolved_predictions.append(pred.resolved_prediction)
        return resolved_predictions
